#!/bin/sh

filename=$1

(
    echo "/^Name=/m1";
    echo "/^Comment=/m2"
    echo "/^Keywords=/m3"; echo w; echo q
) | ed $filename > /dev/null 2>&1
